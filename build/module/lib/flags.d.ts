import { ClientConfig, ModuleConfig } from './proto';
/**
 * Creates a list of flags ready to be consumed by the spawn process.
 * @param config
 */
export declare function parseFlags(config: ClientConfig): string[];
/**
 * Returns a module string that can be passed as a flag value to siad.
 * @param modules
 */
export declare function parseModules(modules: ModuleConfig): string;
