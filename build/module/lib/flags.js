import { assignDefined } from './utils';
/**
 * Creates a list of flags ready to be consumed by the spawn process.
 * @param config
 */
export function parseFlags(config) {
    // Default flags used in the Sia Daemon.
    const defaultFlags = {
        'api-addr': 'localhost:4280',
        'authenticate-api': false,
        'disable-api-security': false,
        'host-addr': ':4282',
        'rpc-addr': ':4281'
    };
    // Build flag arguements from constructor details
    const flags = assignDefined(defaultFlags, {
        agent: config.agent,
        'api-addr': config.apiHost && config.apiPort
            ? `${config.apiHost}:${config.apiPort}`
            : null,
        // If auto, we are going to attempt to resolve api password through the default sia file.
        'authenticate-api': config.apiAuthentication === 'auto' ? null : config.apiAuthentication,
        'host-addr': config.hostPort ? `:${config.hostPort}` : null,
        modules: config.modules ? parseModules(config.modules) : null,
        'rpc-addr': config.rpcPort ? `:${config.rpcPort}` : null,
        'scprime-directory': config.dataDirectory
    });
    // Create flag string list to pass to siad
    const filterFlags = (key) => flags[key] !== false;
    const mapFlags = (key) => `--${key}=${flags[key]}`;
    const flagList = Object.keys(flags)
        .filter(filterFlags)
        .map(mapFlags);
    return flagList;
}
/**
 * Returns a module string that can be passed as a flag value to siad.
 * @param modules
 */
export function parseModules(modules) {
    const moduleMap = {
        consensus: 'c',
        explorer: 'e',
        gateway: 'g',
        host: 'h',
        miner: 'm',
        renter: 'r',
        transactionPool: 't',
        wallet: 'w'
    };
    let enabledModules = '';
    Object.keys(modules).forEach(k => {
        if (modules[k]) {
            enabledModules += moduleMap[k];
        }
    });
    return enabledModules;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmxhZ3MuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvbGliL2ZsYWdzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFFeEM7OztHQUdHO0FBQ0gsTUFBTSxVQUFVLFVBQVUsQ0FBQyxNQUFvQjtJQUM3Qyx3Q0FBd0M7SUFDeEMsTUFBTSxZQUFZLEdBQWM7UUFDOUIsVUFBVSxFQUFFLGdCQUFnQjtRQUM1QixrQkFBa0IsRUFBRSxLQUFLO1FBQ3pCLHNCQUFzQixFQUFFLEtBQUs7UUFDN0IsV0FBVyxFQUFFLE9BQU87UUFDcEIsVUFBVSxFQUFFLE9BQU87S0FDcEIsQ0FBQztJQUNGLGlEQUFpRDtJQUNqRCxNQUFNLEtBQUssR0FBRyxhQUFhLENBQUMsWUFBWSxFQUFFO1FBQ3hDLEtBQUssRUFBRSxNQUFNLENBQUMsS0FBSztRQUNuQixVQUFVLEVBQ1IsTUFBTSxDQUFDLE9BQU8sSUFBSSxNQUFNLENBQUMsT0FBTztZQUM5QixDQUFDLENBQUMsR0FBRyxNQUFNLENBQUMsT0FBTyxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUU7WUFDdkMsQ0FBQyxDQUFDLElBQUk7UUFDVix5RkFBeUY7UUFDekYsa0JBQWtCLEVBQ2hCLE1BQU0sQ0FBQyxpQkFBaUIsS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLGlCQUFpQjtRQUN2RSxXQUFXLEVBQUUsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUk7UUFDM0QsT0FBTyxFQUFFLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUk7UUFDN0QsVUFBVSxFQUFFLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJO1FBQ3hELG1CQUFtQixFQUFFLE1BQU0sQ0FBQyxhQUFhO0tBQzFDLENBQUMsQ0FBQztJQUNILDBDQUEwQztJQUMxQyxNQUFNLFdBQVcsR0FBRyxDQUFDLEdBQVcsRUFBRSxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEtBQUssQ0FBQztJQUMxRCxNQUFNLFFBQVEsR0FBRyxDQUFDLEdBQVcsRUFBRSxFQUFFLENBQUMsS0FBSyxHQUFHLElBQUksS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUM7SUFDM0QsTUFBTSxRQUFRLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7U0FDaEMsTUFBTSxDQUFDLFdBQVcsQ0FBQztTQUNuQixHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDakIsT0FBTyxRQUFRLENBQUM7QUFDbEIsQ0FBQztBQUVEOzs7R0FHRztBQUNILE1BQU0sVUFBVSxZQUFZLENBQUMsT0FBcUI7SUFDaEQsTUFBTSxTQUFTLEdBQUc7UUFDaEIsU0FBUyxFQUFFLEdBQUc7UUFDZCxRQUFRLEVBQUUsR0FBRztRQUNiLE9BQU8sRUFBRSxHQUFHO1FBQ1osSUFBSSxFQUFFLEdBQUc7UUFDVCxLQUFLLEVBQUUsR0FBRztRQUNWLE1BQU0sRUFBRSxHQUFHO1FBQ1gsZUFBZSxFQUFFLEdBQUc7UUFDcEIsTUFBTSxFQUFFLEdBQUc7S0FDWixDQUFDO0lBRUYsSUFBSSxjQUFjLEdBQUcsRUFBRSxDQUFDO0lBQ3hCLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFO1FBQy9CLElBQUksT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQ2QsY0FBYyxJQUFJLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNoQztJQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0gsT0FBTyxjQUFjLENBQUM7QUFDeEIsQ0FBQyJ9